package org.compain.clientui.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Borrowing {

    private Long idBorrowing;
    private String title;
    private String author;
    private LocalDateTime borrowingDate;
    private LocalDateTime borrowingLimitDate;
    private Boolean renewal;
    private Boolean returned;
    private Boolean renewable;

}
