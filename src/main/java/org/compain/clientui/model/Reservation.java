package org.compain.clientui.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Reservation {
    private Long idReservation;
    private Long idUser;
    private Long idBook;
    private String title;
    private LocalDateTime reservationDate;
    private Boolean notified;
    private LocalDateTime notificationDate;
}
