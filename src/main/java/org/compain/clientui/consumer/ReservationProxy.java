package org.compain.clientui.consumer;

import org.compain.clientui.model.Borrowing;
import org.compain.clientui.model.InfoReservation;
import org.compain.clientui.model.Reservation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class ReservationProxy {

    @Value("${backend.base-url}")
    private String url;

    private final RestTemplate restTemplate;

    public ReservationProxy(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public List<Reservation> getUserReservation(String token) {
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);
        header.set(HttpHeaders.AUTHORIZATION,"Bearer " + token);
        HttpEntity<String> request = new HttpEntity<>(header);
        ResponseEntity<List<Reservation>> response = restTemplate.exchange(url + "api/reservations/user", HttpMethod.GET, request, new ParameterizedTypeReference<List<Reservation>>() {});
        return  response.getBody();
    }

    public Void createReservation(String token, Reservation reservation) {
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);
        header.set(HttpHeaders.AUTHORIZATION,"Bearer " + token);
        HttpEntity<Reservation> request = new HttpEntity(reservation, header);
        ResponseEntity<Void> response = restTemplate.exchange(url + "api/reservations/create", HttpMethod.POST, request, Void.class);
        return response.getBody();
    }

    public Void deleteReservation(String token, Long idReservation) {
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);
        header.set(HttpHeaders.AUTHORIZATION,"Bearer " + token);
        HttpEntity<Long> request = new HttpEntity(idReservation, header);
        ResponseEntity<Void> response = restTemplate.exchange(url + "api/reservations/delete", HttpMethod.POST, request, Void.class);
        return response.getBody();
    }

    public InfoReservation getInfoReservation(Long idBook, String token){
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);
        header.set(HttpHeaders.AUTHORIZATION,"Bearer " + token);
        HttpEntity<String> request = new HttpEntity(header);
        ResponseEntity<InfoReservation> response = restTemplate.exchange(url + "api/reservations/book/?idBook={idBook}", HttpMethod.GET,request, InfoReservation.class,idBook );
        return response.getBody();
    }


}
