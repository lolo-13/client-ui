package org.compain.clientui.controller;

import org.compain.clientui.model.Book;
import org.compain.clientui.model.FilterResearch;
import org.compain.clientui.model.InfoReservation;
import org.compain.clientui.model.Reservation;
import org.compain.clientui.service.BookService;
import org.compain.clientui.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@Controller
public class BookController {

    @Autowired
    private BookService bookService;
    @Autowired
    private ReservationService reservationService;

    @GetMapping("/api/books")
    public String books(@CookieValue(value = "tokenCookie", defaultValue = "Atta") String token, Model model){
        List<Book> books = bookService.getBooks(token);
        model.addAttribute("books", books);
        return "books";
    }

    @RequestMapping(value="/api/books/book/{idBook}", method = RequestMethod.GET)
    public String getBook(@CookieValue (value = "tokenCookie", defaultValue = "Atta") String token, Model model, @PathVariable Long idBook ){
        Book book = bookService.getBookById(token, idBook);
        InfoReservation infoReservation = reservationService.getBookInfoReservation(idBook, token);
        model.addAttribute("book", book);
        model.addAttribute("reservation", infoReservation);
      return "bookinfo";
    }

    @RequestMapping(value="/api/books/book/reserve", method = RequestMethod.POST)
    public String reserveBook(@CookieValue (value = "tokenCookie", defaultValue = "Atta") String token, Model model, @ModelAttribute("reservation") Reservation reservation ){
    reservationService.createReservation(token, reservation);
        Reservation newReservation = new Reservation();
        List<Reservation> reservations = reservationService.getUserReservation(token);
        List<InfoReservation> infoReservationList = reservationService.getBooksInfoReservation(token, reservations);
        model.addAttribute("reservations", reservations);
        model.addAttribute("infoReservations", infoReservationList);
        model.addAttribute("reservationToDelete", newReservation);
        return "userReservation";
    }

    @RequestMapping(value = "/api/books/search", method = RequestMethod.GET)
    public String searchBooks(Model model) {
        FilterResearch filterResearch = new FilterResearch();
        model.addAttribute("filters", filterResearch);
        return "research";
    }
    @RequestMapping(value = "/api/books/filtered", method = RequestMethod.GET)
    public String filteredBooks(@ModelAttribute("filters")FilterResearch filterResearch, @CookieValue (value = "tokenCookie", defaultValue = "Atta") String token,  Model model) {
        List<Book> books = bookService.searchBooks(token, filterResearch);
        model.addAttribute("books", books);
        return "books";
    }
}

