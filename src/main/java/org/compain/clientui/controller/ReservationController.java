package org.compain.clientui.controller;

import org.compain.clientui.model.Borrowing;
import org.compain.clientui.model.InfoReservation;
import org.compain.clientui.model.Reservation;
import org.compain.clientui.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class ReservationController {

    @Autowired
    private ReservationService reservationService;

    @GetMapping("/api/reservations/user")
    public String reservations(@CookieValue(value = "tokenCookie", defaultValue = "Atta") String token, Model model){
        Reservation reservation = new Reservation();
        List<Reservation> reservations = reservationService.getUserReservation(token);
        List<InfoReservation> infoReservationList = reservationService.getBooksInfoReservation(token, reservations);
        model.addAttribute("reservations", reservations);
        model.addAttribute("infoReservations", infoReservationList);
        model.addAttribute("reservationToDelete", reservation);
        return "userReservation";
    }

    @RequestMapping(value="/api/reservations/delete", method = RequestMethod.POST)
    public String deleteReservations(@CookieValue (value = "tokenCookie", defaultValue = "Atta") String token, @ModelAttribute("reservationToDelete") Reservation reservation, Model model ){
        reservationService.delete(token, reservation);
        List<Reservation> reservations = reservationService.getUserReservation(token);
        model.addAttribute("reservations", reservations);
        return "userReservation";
    }
}
