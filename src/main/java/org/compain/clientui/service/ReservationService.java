package org.compain.clientui.service;

import lombok.Data;
import org.compain.clientui.consumer.ReservationProxy;
import org.compain.clientui.model.InfoReservation;
import org.compain.clientui.model.Reservation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.List;

@Data
@Service
public class ReservationService {

    @Autowired
    private ReservationProxy reservationProxy;

    public InfoReservation getBookInfoReservation (Long idBook, String token){
            return reservationProxy.getInfoReservation(idBook, token);

    }

    public List<Reservation> getUserReservation(String token) {
        return reservationProxy.getUserReservation(token);
    }

    public void delete(String token, Reservation reservation) {
        reservationProxy.deleteReservation(token, reservation.getIdReservation());
    }

    public void createReservation(String token, Reservation reservation) {
        reservationProxy.createReservation(token, reservation);
    }

    public List<InfoReservation> getBooksInfoReservation(String token, List<Reservation> reservations) {
        List<InfoReservation> infoReservationList = new ArrayList<>();
        for (Reservation reservation: reservations
             ) {
          infoReservationList.add(getBookInfoReservation(reservation.getIdBook(), token));

        }
        return infoReservationList;
    }
}
