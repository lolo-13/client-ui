<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="java.time.format.DateTimeFormatter" %>

<!DOCTYPE HTML>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="/css/style.css" type="text/css" />

        <title>mes emprunts</title>
    </head>

    <body>
	    <header>
		    <%@include file="header.jsp"%>
	    </header>
	   <div class = "contour">
            <div class="card-deck">
                <c:forEach var="reservation" items="${reservations}" varStatus="status">
                        <form:form  method="post" action="/api/reservations/delete" modelAttribute="reservationToDelete" cssStyle="padding:10px">
                            <div class="card bg-light">
                                <div class="card-header text-center text-info font-weight-bold">
                                    <c:out value="${reservation.title}" />
                                </div>
                                <div class="card-body text-center">
                                    <p class="card-text">
                                      prochain retour préu le :
                                         <c:forEach var="infoReservation" items="${infoReservations}" varStatus="status">
                                                <c:if test="${infoReservation.idBook == reservation.idBook}">
                                                 ${infoReservation.firstReturn.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"))}
                                                </c:if>
                                         </c:forEach>
                                    </p>
                                    <p class="card-text">
                                     Nombre de réservation :
                                         <c:forEach var="infoReservation" items="${infoReservations}" varStatus="status">
                                                <c:if test="${infoReservation.idBook == reservation.idBook}">
                                                 ${infoReservation.currentReservation}
                                                </c:if>
                                         </c:forEach>
                                    </p>
                                    <p class="card-text">
                                     Ma position :
                                         <c:forEach var="infoReservation" items="${infoReservations}" varStatus="status">
                                                <c:if test="${infoReservation.idBook == reservation.idBook}">
                                                 ${infoReservation.myPosition}
                                                </c:if>
                                         </c:forEach>
                                    </p>
                                    <p class="card-text">
                                        <form:hidden path="idReservation" value = "${reservation.idReservation}"/>
                                        <input class="btn btn-secondary" type="SUBMIT" value="Annuler réservation" />
                                    </p>
                                </div>
                            </div>
                        </form:form>
                </c:forEach>
            </div>
       </div>
    </body>
</html>